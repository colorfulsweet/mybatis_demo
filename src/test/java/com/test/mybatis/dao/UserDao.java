package com.test.mybatis.dao;

import java.util.List;

import com.test.mybatis.entity.User;

public interface UserDao {
	
	public User get(String id);
	
	public List<User> findAll();
	
	public int insert(User user);
	
	public int update(User user);
	
	public int delete(String id);
}
