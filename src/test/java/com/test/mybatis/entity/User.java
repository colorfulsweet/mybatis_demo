package com.test.mybatis.entity;

public class User {
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", realname=" + realname + ", age=" + age + "]";
	}
	private String id;
	private String username;
	private String realname;
	private int age;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
