package com.test.unit;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.test.mybatis.dao.UserDao;
import com.test.mybatis.entity.User;

public class MybatisTest {
	private UserDao userDao;
	private SqlSession session;
	@Before
	public void before() {
		PropertyConfigurator.configure("src/test/resources/log4j.properties");
		try {
			//读取配置文件
			Reader reader = Resources.getResourceAsReader("mybatis_config.xml");
			//创建sqlsession工厂
			SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(reader);
			//开启sqlsession
			session = sessionFactory.openSession();
			//获取dao层对象
			userDao = session.getMapper(UserDao.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@After
	public void after() {
		session.close();
	}
	
	
	@Test
	public void Test() {
		User user = userDao.get("1001");
		System.out.println(user);
	}
}
